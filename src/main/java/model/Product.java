package model;

import java.util.Objects;

import javafx.beans.property.*;

public class Product {
    public int getId() {
        return id.get();
    }

    public void setId(int id) {
        this.id.set(id);
    }

    private final SimpleIntegerProperty id;

    public String getProducto() {
        return producto.get();
    }

    public void setProducto(String producto) {
        this.producto.set(producto);
    }

    public String getDescripcion() {
        return descripcion.get();
    }

    public void setDescripcion(String descripcion) {
        this.descripcion.set(descripcion);
    }

    public String getCategoria() {
        return categoria.get();
    }

    public void setCategoria(String categoria) {
        this.categoria.set(categoria);
    }


    private final SimpleStringProperty producto;
    private final SimpleStringProperty descripcion;
    private final SimpleStringProperty categoria;

    public double getPrecio() {
        return precio.get();
    }

    public void setPrecio(double precio) {
        this.precio.set(precio);
    }

    private final SimpleDoubleProperty precio;
    
    public Product(){
        this(0, "", "", "", 0);
    }
    
    public Product(int id, String producto, String descripcion, String categoria, double precio) {
        this.id =  new SimpleIntegerProperty(id);
        this.producto = new SimpleStringProperty(producto);
        this.descripcion = new SimpleStringProperty(descripcion);
        this.categoria = new SimpleStringProperty(categoria);
        this.precio = new SimpleDoubleProperty(precio);
    }

    public IntegerProperty idProperty(){return id;}
    public StringProperty productoProperty() {return producto;}
    public StringProperty descripcionProperty() {return descripcion;}
    public StringProperty categoriaProperty() {return categoria;}
    public SimpleDoubleProperty precioProperty(){return precio;}


    @Override
    public int hashCode() {
        int hash = 3;
        hash = 89 * hash + Objects.hashCode(this.id);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Product other = (Product) obj;
        if (!Objects.equals(this.id, other.id)) {
            return false;
        }
        return true;
    }

}
