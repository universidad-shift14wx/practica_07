package model;

import java.util.List;

public class ProductoRepository {

    public static List<Product> getLibros() {
        return List.of(
        new Product(1, "Cebolla", "esta es una cebolla", "Verduras", 20.0),
        new Product(2, "Libra de arroz", "Este es una libra de arroz", "perecederos", 30.0),
        new Product(3, "Uvas", "son uvas frescas", "Perecederos", 40.3),
        new Product(4, "Zanahorias", "Son zanahorias, fresquitas", "Verduras", 50.69)
        );
    }
}
