package helpers;

import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.paint.Color;

public class TextFieldValidation {

    public static boolean isTextFieldEmpty(TextField textField) {
        boolean res = false;
        if (textField.getText().length() == 0 || textField.getText().isEmpty()) {
            res = true;
        }
        return res;
    }

    public static boolean isTextFieldEmpty(TextField textField, Label errorLabel, String errorMessage) {
        boolean res = false;
        Color color = Color.BLACK;
        String msg = null;
        textField.getStyleClass().remove("error");
        errorLabel.setTextFill(Color.RED);
        if (isTextFieldEmpty(textField)) {
            color = Color.RED;
            msg = errorMessage;
            textField.getStyleClass().add("error");
            errorLabel.setTextFill(Color.BLACK);
            res = true;
        }
        errorLabel.setText(msg);
        errorLabel.setTextFill(color);
        return res;
    }

    public static boolean isNotDouble(TextField textField, Label errorLabel, String errorMessage) {
        boolean res = false;
        Color color = Color.BLACK;
        String msg = null;
        textField.getStyleClass().remove("error");
        try {
            Double.valueOf( textField.getText() );
        }catch (Exception e){
            color = Color.RED;
            msg = errorMessage;
            textField.getStyleClass().add("error");
            res = true;
        }
        errorLabel.setText(msg);
        errorLabel.setTextFill(color);
        return res;
    }

    public static boolean isNotReMatch(TextField textField, String re, Label errorLabel, String errorMessage) {
        boolean res = false;
        Color color = Color.BLACK;
        String msg = null;
        textField.getStyleClass().remove("error");
        errorLabel.getStyleClass().remove("lblerror");
        if (!textField.getText().matches(re)) {
            color = Color.RED;
            msg = errorMessage;
            textField.getStyleClass().add("error");
            errorLabel.getStyleClass().add("lblerror");
            res = true;
        }
        errorLabel.setText(msg);
        errorLabel.setTextFill(color);
        return res;
    }

    public static boolean isComboBoxEmpty(ComboBox comboBox) {
        boolean res = false;
        if (comboBox.getValue() == "") {
            res = true;
        }
        return res;
    }

    public static boolean isComboBoxEmpty(ComboBox comboBox, Label errorLabel, String errorMessage) {
        boolean res = false;
        Color color = Color.BLACK;
        String msg = null;
        comboBox.getStyleClass().remove("error");
        if (isComboBoxEmpty(comboBox)) {
            color = Color.RED;
            msg = errorMessage;
            comboBox.getStyleClass().add("error");
            res = true;
        }
        errorLabel.setText(msg);
        errorLabel.setTextFill(color);
        return res;
    }

    public static void restrictNumbersOnly(KeyEvent keyEvent) {
        switch (keyEvent.getCode()) {
            case TAB:
            case BACK_SPACE:
            case DELETE:
                break;
            default:
                if (!keyEvent.getCode().isDigitKey()) {
                    Dialogs.showErrorDialog("Error", "Incorrecto", "Ingrese solamente digitos");
                    keyEvent.consume();
                }
        }
    }

    public static void restrictLettersOnly(KeyEvent keyEvent) {
        KeyCode code = keyEvent.getCode();
        switch (code) {
            case TAB:
            case BACK_SPACE:
            case DELETE:
            case SPACE:
            case SHIFT:
                break;
            default:
                if (!code.isLetterKey()) {
                    Dialogs.showErrorDialog("Error", "Incorrecto", "Ingrese solamente letras y espacios");
                    keyEvent.consume();
                }
        }
    }

}
