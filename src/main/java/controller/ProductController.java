package controller;

import helpers.Dialogs;
import java.net.URL;
import java.util.ResourceBundle;

import helpers.TextFieldValidation;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.paint.Color;
import model.Categorias;
import model.Product;

public class ProductController implements Initializable {

    @FXML
    private DialogPane dialogPane;
    @FXML
    private TextField id;
    @FXML
    private TextField producto;
    @FXML
    private TextField descripcion;
    @FXML
    private ComboBox<String> categorias;
    @FXML
    private TextField precio;


    // labels

    @FXML
    private Label errorCode;
    @FXML
    private Label errorProducto;
    @FXML
    private Label errorDescp;
    @FXML
    private Label errorCategories;
    @FXML
    private Label errorPrice;

    private ObservableList<String> ListCategories = null;
    private Product product = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        ListCategories = FXCollections.observableArrayList(Categorias.getCategorias());
        categorias.setItems(ListCategories);

        this.validateInputs();

    }
    
    public void setProduct(Product product) {
        this.product = product;
        id.setText(Integer.toString(product.getId()));
        producto.setText(product.getProducto());
        descripcion.setText(product.getDescripcion());
        categorias.setValue(product.getCategoria());
        precio.setText(Double.toString(product.getPrecio()));
        if (id.getText().equals("0")) {
            id.clear();
            precio.clear();
        }

        Button okButton = (Button) dialogPane.lookupButton(ButtonType.OK);
        okButton.addEventFilter(ActionEvent.ACTION, event -> {

            // validar campos
            boolean v1 = TextFieldValidation.isTextFieldEmpty(id, errorCode, "Debe ingresar el codigo");
            boolean v2 = TextFieldValidation.isTextFieldEmpty(producto, errorProducto, "Debe ingresar el título");
            boolean v3 = TextFieldValidation.isTextFieldEmpty(descripcion, errorDescp, "Debe ingresar el autor");
            boolean v4 = TextFieldValidation.isComboBoxEmpty(categorias, errorCategories, "Debe seleccionar la editorial");
            boolean v5 = TextFieldValidation.isNotDouble(precio, errorPrice, "Debe ingresar el año correctamente");

//            errorCode.setTextFill(v1 ? Color.RED : Color.BLACK);
//            errorProducto.setTextFill(v2 ? Color.RED : Color.BLACK);
//            errorDescp.setTextFill(v3 ? Color.RED : Color.BLACK);
//            errorCategories.setTextFill(v4 ? Color.RED : Color.BLACK);
//            errorPrice.setTextFill(v5 ? Color.RED : Color.BLACK);

            if (v1 || v2 || v3 || v4 || v5) {
                event.consume();
            } else {
            try {
                product.setId(Integer.parseInt(id.getText()));
                product.setProducto(producto.getText());
                product.setDescripcion(descripcion.getText());
                product.setCategoria(categorias.getValue());
                product.setPrecio(Double.parseDouble(precio.getText()));
            } catch (NumberFormatException e) {
                Dialogs.showErrorDialog("Error", null, "Todos los datos son requeridos o debe usar el formato correcto.");
                event.consume();
            }
        }
        });
    }


    // validar los inputs

    void validateInputs(){
        id.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d*")) {
                id.setText(newValue.replaceAll("[^a-zA-Z0-9]", ""));
            }
        });
    }

}
