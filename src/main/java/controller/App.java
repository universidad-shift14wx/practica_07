package controller;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class App extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        /* Vista productos */
        Parent root = FXMLLoader.load(getClass().getResource("/view/Products.fxml"));
        root.getStylesheets().add(getClass().getResource("/css/style.css").toString());
        stage.setScene(new Scene(root, 840, 480));
        stage.setResizable(false);
        stage.setTitle("Productos");
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}
