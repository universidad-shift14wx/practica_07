package controller;

import helpers.Dialogs;
import java.io.IOException;
import java.net.URL;
import java.util.Optional;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.DialogPane;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import model.Product;
import model.ProductoRepository;

enum DialogMode {
    ADD, UPDATE
}

public class productsController implements Initializable {

    @FXML
    private Button btnAgregar;
    @FXML
    private Button btnEliminar;
    @FXML
    private Button btnActualizar;
    @FXML
    private TableView<Product> tbProductos;
    @FXML
    private TableColumn<Product, Integer> colId;
    @FXML
    private TableColumn<Product, String> colProducto;
    @FXML
    private TableColumn<Product, String> colDescripcion;
    @FXML
    private TableColumn<Product, String> colCategoria;
    @FXML
    private TableColumn<Product, Integer> colPrecio;

    private ObservableList<Product> listaProducts = null;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        listaProducts = FXCollections.observableArrayList(ProductoRepository.getLibros());
        colId.setCellValueFactory(new PropertyValueFactory("id"));
        colProducto.setCellValueFactory(new PropertyValueFactory("producto"));
        colDescripcion.setCellValueFactory(new PropertyValueFactory("descripcion"));
        colCategoria.setCellValueFactory(new PropertyValueFactory("categoria"));
        colPrecio.setCellValueFactory(new PropertyValueFactory("precio"));
        tbProductos.setItems(listaProducts);
        tbProductos.setPlaceholder(new Label("No hay products para mostrar"));
        
        btnActualizar.disableProperty().bind(
                tbProductos.getSelectionModel().selectedItemProperty().isNull());

        btnEliminar.disableProperty().bind(
                tbProductos.getSelectionModel().selectedItemProperty().isNull());
    }

    @FXML
    private void handleActualizar(ActionEvent event) {
        handleAgregar(event);
    }

    @FXML
    private void handleAgregar(ActionEvent event) {
        Product product = null;
        String dialogTitle = "";
        DialogMode mode;

        if (event.getSource().equals(btnActualizar)) {
            mode = DialogMode.UPDATE;
            dialogTitle = "Actualizar producto";
            product = tbProductos.getSelectionModel().getSelectedItem();
        } else if (event.getSource().equals(btnAgregar)) {
            mode = DialogMode.ADD;
            dialogTitle = "Nuevo producto";
            product = new Product();
        } else {
            return;
        }

        try {
            FXMLLoader fxmlLoader = new FXMLLoader();
            fxmlLoader.setLocation(getClass().getResource("/view/ProductForm.fxml"));
            DialogPane productDialogPane = fxmlLoader.load();
            
            ProductController productController = fxmlLoader.getController();
            productController.setProduct(product);
            
            Dialog<ButtonType> dialog = new Dialog<>();
            dialog.setDialogPane(productDialogPane);
            dialog.setTitle(dialogTitle);
            Optional<ButtonType> clickedButton = dialog.showAndWait();
            if (clickedButton.get() == ButtonType.OK) {
                if (mode == DialogMode.ADD) {
                    listaProducts.add(product);
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    @FXML
    private void handleEliminar(ActionEvent event) {
        Product product = tbProductos.getSelectionModel().getSelectedItem();
        Optional<ButtonType> result
                = Dialogs.showConfirmationDialog("Confirmación", null,
                        "¿Desea eliminar el producto id " + product.getId() + "?");
        if (result.get() == ButtonType.YES) {
            listaProducts.remove(product);
            tbProductos.getSelectionModel().select(null);
        }
    }

}
